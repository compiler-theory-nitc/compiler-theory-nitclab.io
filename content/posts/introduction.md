---
title: "Introduction"
date: 2020-06-13T13:22:14+05:30
draft: true
---

This is a simple website to curate questions and answers regarding Compiler
Theory course taught in National Institute of Technology, Calicut.

The questions will be collected over email by student volunteers.
And will be consolidated and added to the site at the earliest.

The website is specifically designed so that it loads under one second even
in 2G network. This is to make sure that no student even in the remote
areas are left out. We will also periodically collect all the content in a
pdf, so that it can be viewed offline or can be printed and kept.

